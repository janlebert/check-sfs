# Copyright (C) 2019-2020 Research Group Biomedical Physics,
# Max-Planck-Institute for Dynamics and Self-Organization Göttingen
# Authors:
# A. Schlemmer

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import argparse
from .check_sfs import check_sfs


def main():
    parser = argparse.ArgumentParser(
        description="Check validity of a folder according to the specification.")

    parser.add_argument("glob",
                        help="A glob according to the python glob specification,"
                             " identifying directories to process.")
    parser.add_argument("-e", "--exclude", nargs='+',
                        help="list of directories to exclude from the check. Path names"
                             " should be given relative to the glob")
    parser.add_argument("-i", "--interactive",
                        help="Interactive mode, ask for missing information and create"
                             " missing headers automatically", action="store_true")

    args = parser.parse_args()
    m = check_sfs(args.glob, exclude=args.exclude)
    for r in m:
        if len(r[2]) > 0:
            print()
            print(r[0])
            for rr in r[2]:
                print("    ", rr)


if __name__ == "__main__":
    main()
