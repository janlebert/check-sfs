---
responsible: AuthorA
description: Average over all data of each type of experiment separately and comined.
sources:
- ../../../ExperimentalData/2020_SpeedOfLight/2020-01-01_TimeOfFlight
- ../../../ExperimentalData/2020_SpeedOfLight/2020-01-02_Cavity
- ../../../ExperimentalData/2020_SpeedOfLight/2020-01-03/velocities.txt
results:
- file: single-averages-*.csv
  description: average speed of light from all single types of measurements
- file: all-averages.csv
  description: average speed of light from all measurements combined
- file: "*.pdf"
  description: Plots of the averages
scripts:
- file: calculate-averages.py
  description: python code doing the calculation
- file: plot-averages.py
  description: create nice plots for article
...
