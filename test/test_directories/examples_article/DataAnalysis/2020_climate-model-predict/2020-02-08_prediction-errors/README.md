---
responsible: AuthorD
description: comparison between predicted and measured temperatures for 2010 to 2019
sources:
- ../../../ExperimentalData/2020_climate-model-predict/2010-01-01/temperatures-*.csv
- ../../../SimulationData/2020_climate-model-predict/2020-02-01/predictions-*.csv
results:
- file: "*.pdf"
  description: Plots of absolute and relative errors
- file: errors.csv
  description: prediction errors for all measurement locations
scripts:
- file: differences.py
  description: Calculate the absolute and relative differences between predicted and measured temperatures, and plot them.
...
