---
responsible:
- AuthorA
- AuthorB
- AuthorC
description: Article on the comparison of several experimental methods for determining the speed of light.
sources:
- ../../../ExperimentalData/2020_SpeedOfLight/2020-01-01_TimeOfFlight
- ../../../ExperimentalData/2020_SpeedOfLight/2020-01-02_Cavity
- ../../../ExperimentalData/2020_SpeedOfLight/2020-01-03/velocities.txt
- ../../../DataAnalysis/2020-01-05_average-all-exp-corr
...

# Further Notes

  The corrected analysis was used in Figure 1.