---
responsible: AuthorD
description: beamer slides of the conference talk given at the 2020 climate modeling conference in Berlin
sources:
- ../../../ExperimentalData/2020_climate-model-predict/1980-01-01/temperatures-*.csv
- ../../../ExperimentalData/2020_climate-model-predict/1990-01-01/temperatures-*.csv
- ../../../ExperimentalData/2020_climate-model-predict/2000-01-01/temperatures-*.csv
- ../../../ExperimentalData/2020_climate-model-predict/2010-01-01/temperatures-*.csv
- ../../../SimulationData/2020_climate-model-predict/2020-02-01
- ../../../DataAnalysis/2020_climate-model-predict/2020-02-08_prediction-errors
...
